|=============================================
| Important Links
|=============================================
https://javaconceptoftheday.com/
http://www.springboottutorial.com/
https://javadeveloperzone.com/
https://spring.io/s
https://www.callicoder.com/
https://memorynotfound.com/
https://o7planning.orgs

|=============================================
| Maven Cammands
|=============================================
-- mvn --version
# creating project // This archetype:generate goal created a simple project based upon a maven-archetype-quickstart archetype.
-- mvn archetype:generate -DgroupId=com.mycompany.app -DartifactId=my-app -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.4 -DinteractiveMode=false
-- cd my-app
# build package
-- mvn package
# run the project
-- java -cp target/my-app-1.0-SNAPSHOT.jar com.mycompany.app.App
# Maven Phases
1.validate, 2.compile, 3.test, 4.package, 5.integration-test, 6.verify, 7.install, 8. deploy
validate: validate the project is correct and all necessary information is available
compile: compile the source code of the project
test: test the compiled source code using a suitable unit testing framework. These tests should not require the code be packaged or deployed
package: take the compiled code and package it in its distributable format, such as a JAR.
integration-test: process and deploy the package if necessary into an environment where integration tests can be run
verify: run any checks to verify the package is valid and meets quality criteria
install: install the package into the local repository, for use as a dependency in other projects locally
deploy: done in an integration or release environment, copies the final package to the remote repository for sharing with other developers and projects.
-----------------
clean: cleans up artifacts created by prior builds
site: generates site documentation for this project

|==============================
|What is the DTO Design Pattern?
|==============================
if multiple requests are required to bring data for a particular task, data to be brought can be combined in a DTO so that only one request can bring all the required data.
|===================================
|When is the DTO Design Pattern used?
|===================================
- simple apps
		domain objects can be often be directly reused as DTOs
- complex apps
		mapping from domain models to DTOs is necessary
|=============================================================
|When to used primitive type and when to use wrapper classes ?
|=============================================================
when use primitive then use present for boolean, when use wrapper class use isPresent

|==================================================
|when to use javax.validate or javax.presistance ?
|==================================================


|================================
|Terminal Cammands
|================================
- lsof -i -P # for process id and ports
- kill -9 <PI> # for delete the process
	
|================================
|New to Me
|================================


|=============================================
| Issues
|=============================================
01. No serializer found for class org.hibernate.proxy.pojo.javassist.Javassist?
Ans: The problem is that entities are loaded lazily and serialization happens before they get loaded fully.
- spring.jackson.serialization.fail-on-empty-beans=false

|=============================================
| Eureka
|=============================================
-- Netflix OSS (one of the leader in microservice library)
-- library  (Eureka, Ribbon, Hysterix, Zuul)
-- client --> Eureka-Server(Discovery-server) <-- (Eureka client)
-- Steps to making this work
	- start up a Eureka server
	- Have microservices rigister(pulish) using Eureka client
	- Have microservices locate(consume) using Eureka client
-- create a spring boot using spring-initalizer 
 -- add two dependency name 'Eureka Server, Eureka Discoverys'
 @EnableEurekaServers
 -- when using java 11 might got some error so include some 'jaxb'