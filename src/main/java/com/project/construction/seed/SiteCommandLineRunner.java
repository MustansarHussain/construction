package com.project.construction.seed;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import com.project.construction.model.Site;
import com.project.construction.repository.SiteRepository;

@Component
public class SiteCommandLineRunner implements CommandLineRunner, Ordered {
	
	@Autowired
	private SiteRepository siteRepo;
	
	private final Logger logger = LoggerFactory.getLogger(SiteCommandLineRunner.class);
 
    @Override
    public void run(String... args) throws Exception {
    /*	
        siteRepo.deleteAllInBatch();
        
        siteRepo.save(new Site(1l, "CHATRAL LANE"));
        siteRepo.save(new Site(2l, "68/6 QTR"));
        siteRepo.save(new Site(3l, "POL"));
        siteRepo.save(new Site(4l, "CHATRAL LANE"));
        siteRepo.save(new Site(5l, "89 ABID MAJEED"));
        siteRepo.save(new Site(6l, "LALYANI"));
        siteRepo.save(new Site(7l, "85 ABID MAJEED"));
        siteRepo.save(new Site(8l, "C 3"));
        siteRepo.save(new Site(9l, "MIAN MEER"));
        siteRepo.save(new Site(10l, "BRK 16"));
        siteRepo.save(new Site(11l, "A3"));
        siteRepo.save(new Site(12l, "8 QTR"));
       
        */
        logger.info("Default Table for Site is set");
    }
 
    @Override
    public int getOrder() {
        return 2;
    }
}
