package com.project.construction.seed;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.project.construction.repository.EmployeeRepository;

@Component
@Order(1)
public class EmployeeCommandLineRunner implements CommandLineRunner {

	@Autowired
	EmployeeRepository employeeRepo;

	private final Logger logger = LoggerFactory.getLogger(EmployeeCommandLineRunner.class);

	@Override
	public void run(String... args) throws Exception {

		/*
		 * 
		 * employeeRepo.deleteAllInBatch();
		 * 
		 * 
		 * employeeRepo.save(new Employee(1l, "Boota", "M Nazair", "35202-1234567-9",
		 * 850.0)); employeeRepo.save(new Employee(2l, "RAFQAT", "M Nazair",
		 * "35202-1234567-9", 850.0)); employeeRepo.save(new Employee(3l, "ALLAH RAKHA",
		 * "M Nazair", "35202-1234567-9", 850.0)); employeeRepo.save(new Employee(4l,
		 * "IRFAN AKHTAR", "M Nazair", "35202-1234567-9", 550.0)); employeeRepo.save(new
		 * Employee(5l, "SHAHZAD CHANU", "M Nazair", "35202-1234567-9", 550.0));
		 * employeeRepo.save(new Employee(6l, "IRFAN", "M Nazair", "35202-1234567-9",
		 * 550.0));
		 * 
		 */
		logger.info("Default employee is set");
	}
}
