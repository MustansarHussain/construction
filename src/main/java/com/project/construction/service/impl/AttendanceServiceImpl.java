package com.project.construction.service.impl;

import com.project.construction.dto.AttendanceDto;
import com.project.construction.exception.ResourceNotFoundException;
import com.project.construction.model.Attendance;
import com.project.construction.model.Employee;
import com.project.construction.model.Site;
import com.project.construction.repository.AttendanceRepository;
import com.project.construction.repository.EmployeeRepository;
import com.project.construction.repository.SiteRepository;
import com.project.construction.service.AttendanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AttendanceServiceImpl implements AttendanceService {

	@Autowired
	SiteRepository siteRepo;
	
	@Autowired
	EmployeeRepository employeeRepo;

	@Autowired
	AttendanceRepository attendanceRepo;

    public List<Attendance> getAllAttendanceByEmployeeId(Long employeeId){
        return attendanceRepo.findByEmployeeId(employeeId);
    }


    public Attendance createAttendanceOfEmployee(Long employeeId, AttendanceDto attendanceDto){
    	
    	// get employee
    	Optional<Employee> optinalEmployee = employeeRepo.findById(employeeId);
    	Employee employee = optinalEmployee.get();
    	
    	// get Site
    	Optional<Site> optionalSite = siteRepo.findById( attendanceDto.getSiteId() );
    	Site site = optionalSite.get();
    	
    	// create attendance
    	Attendance attendance = new Attendance();
   
    	attendance.setSite(site);
    	attendance.setIspresent(true);
    	attendance.setEmployee( employee );
    	attendance.setSalary(employee.getSalary());
    	attendance.setRemarks(attendanceDto.getRemarks());
    	attendance.setOverTime(attendanceDto.getOverTime());
    	attendance.setAdvanceAmount(attendanceDto.getAdvanceAmount());
    	attendance.setAttendanceDate(attendanceDto.getAttendanceDate());
  
    	return attendanceRepo.save(attendance);
    }
    
    public Attendance updateAttendanceOfEmployeeId(Long employeeId, Long attendanceId, Attendance attendanceDetails){
    	if(!employeeRepo.existsById(employeeId))
    		throw new ResourceNotFoundException("Employee", "employeeId", "not found");
    	return null;
    }
    
}
