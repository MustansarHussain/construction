package com.project.construction.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.project.construction.exception.ResourceNotFoundException;
import com.project.construction.model.Employee;
import com.project.construction.repository.EmployeeRepository;
import com.project.construction.service.EmployeeService;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	EmployeeRepository employeeRepo;

	// Get All Employee
	public List<Employee> getAllEmployees() throws ResourceNotFoundException {
		return employeeRepo.findAll();
	}

	// Create a new Employee
	@Transactional
	public Employee createEmployee(Employee employee) throws ResourceNotFoundException {
		return employeeRepo.save(employee);
	}

	// Get a Single Employee
	public Employee getEmployeeById(Long employeeId) {
		return employeeRepo.findById(employeeId)
				.orElseThrow(() -> new ResourceNotFoundException("Note", "id", employeeId));
	}

	// Update a Employee
	public Employee updateEmployee(Long employeeId, Employee employeeDetails) {

		Employee employee = employeeRepo.findById(employeeId)
				.orElseThrow(() -> new ResourceNotFoundException("employee", "id", employeeId));

		employee.setName(employeeDetails.getName());
		employee.setFatherName(employeeDetails.getFatherName());
		employee.setCnic(employeeDetails.getCnic());
		employee.setSalary(employeeDetails.getSalary());

		Employee updatedemployee = employeeRepo.save(employee);
		return updatedemployee;
	}

	// Delete a Employee
	public ResponseEntity<?> deleteEmployee(Long employeeId, Employee employeeDetails) {

		Employee employee = employeeRepo.findById(employeeId)
				.orElseThrow(() -> new ResourceNotFoundException("employee", "id", employeeId));

		employeeRepo.delete(employee);

		return ResponseEntity.ok().build();
	}
}
