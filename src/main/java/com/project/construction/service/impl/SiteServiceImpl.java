package com.project.construction.service.impl;

import com.project.construction.exception.ResourceNotFoundException;
import com.project.construction.model.Site;
import com.project.construction.repository.SiteRepository;
import com.project.construction.service.SiteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SiteServiceImpl implements SiteService {

    @Autowired
    SiteRepository siteRepo;

    // Get All Site
    public List<Site> getAllSites(){
        return siteRepo.findAll();
    }

    // Create a new Site
    public Site createSite(Site Site){
        return siteRepo.save(Site);
    }

    // Get a Single Site
    public Site getSiteByID(Long siteId){
        return siteRepo.findById(siteId)
                .orElseThrow(() -> new ResourceNotFoundException("Site", "id", siteId));
    }

    // Update a Site
    public Site updateSite(Long siteId, Site siteDetails){
        Site site = siteRepo.findById(siteId)
                .orElseThrow(() -> new ResourceNotFoundException("Site", "id", siteId));

        site.setName(siteDetails.getName());

        Site updatedSite = siteRepo.save(site);
        return updatedSite;
    }

    // Delete a Site
    public ResponseEntity<?> deleteSite(Long siteId){
        Site site = siteRepo.findById(siteId)
                .orElseThrow(() -> new ResourceNotFoundException("site", "id", siteId));

        siteRepo.delete(site);

        return ResponseEntity.ok().build();
    }
}
