package com.project.construction.service;

import com.project.construction.model.Site;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface SiteService {

    public List<Site> getAllSites();
    public Site createSite(Site Site);
    public Site getSiteByID(Long siteId);
    public Site updateSite(Long siteId, Site siteDetails);
    public ResponseEntity<?> deleteSite(Long siteId);
}
