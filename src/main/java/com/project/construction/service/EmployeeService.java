package com.project.construction.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.project.construction.model.Employee;

public interface EmployeeService {

	// Get All Employees
	public List<Employee> getAllEmployees();

	// Create a Employee
	public Employee createEmployee(Employee employee);

	// Get Employee by Id
	public Employee getEmployeeById(Long employeeId);

	// Update a Employee
	public Employee updateEmployee(Long employeeId, Employee employeeDetails);

	// Delete a employee
	public ResponseEntity<?> deleteEmployee(Long employeeId, Employee employeeDetails);
}
