package com.project.construction.service;

import java.util.List;

import com.project.construction.dto.AttendanceDto;
import com.project.construction.model.Attendance;

public interface AttendanceService {

	// list all Attendance of a specific employee
	public List<Attendance> getAllAttendanceByEmployeeId(Long employeeId);

	// create attendace
	public Attendance createAttendanceOfEmployee(Long employeeId, AttendanceDto attendanceDto);

	// update the Attendance
	public Attendance updateAttendanceOfEmployeeId(Long employeeId, Long attendanceId, Attendance attendanceDetails);

}
