package com.project.construction.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.construction.dto.AttendanceDto;
import com.project.construction.model.Attendance;
import com.project.construction.service.AttendanceService;

@RestController
@RequestMapping("/api")
public class AttendanceController {

	@Autowired
	private AttendanceService attendanceService;

	@GetMapping("/employees/{id}/attendance")
	public List<Attendance> get(@PathVariable(value = "id") Long employeeId) {
		return attendanceService.getAllAttendanceByEmployeeId(employeeId);
	}

	@PostMapping("/employees/{id}/attendance")
	public Attendance create(@PathVariable(value = "id") Long employeeId,
			@Valid @RequestBody AttendanceDto attendanceDto) {
		return attendanceService.createAttendanceOfEmployee(employeeId, attendanceDto);
	}

	@PutMapping("/employees/{employeeId}/attendance/{attendanceId}")
	public Attendance update(@PathVariable(value = "employeeId") Long employeeId,
			@PathVariable(value = "attendanceId") Long attendanceId, Attendance attendanceDetails) {
		return null;
	}
}
