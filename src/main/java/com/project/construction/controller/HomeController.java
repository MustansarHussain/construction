package com.project.construction.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

	@GetMapping("/index")
	public String index() {
		return "index";
	}

	@GetMapping("/employee")
	public String get_employee() {
		return "employee/get";
	}

	@GetMapping("/employee/create")
	public String create_employee() {
		return "employee/create";
	}
}
