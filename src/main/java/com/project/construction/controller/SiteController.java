package com.project.construction.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.project.construction.exception.ResourceNotFoundException;
import com.project.construction.model.Site;
import com.project.construction.service.SiteService;

@RestController
@RequestMapping("/api")
public class SiteController {
	@Autowired
	private SiteService siteService;

	@GetMapping("/sites")
	public List<Site> getAllSites() throws ResourceNotFoundException {
		return siteService.getAllSites();
	}

	@PostMapping("/sites")
	public Site createSite(@Valid @RequestBody Site site) {
		return siteService.createSite(site);
	}

	@GetMapping("/sites/{id}")
	public Site getSiteByID(@PathVariable(value = "id") Long siteId) {
		return siteService.getSiteByID(siteId);
	}

	@PutMapping("/sites/{id}")
	public Site updateSite(@PathVariable(value = "id") Long siteId, @Valid @RequestBody Site siteDetails) {

		return siteService.updateSite(siteId, siteDetails);
	}

	@DeleteMapping("/sites/{id}")
	public ResponseEntity<?> deleteSite(@PathVariable(value = "id") Long siteId) {
		return siteService.deleteSite(siteId);
	}
}
