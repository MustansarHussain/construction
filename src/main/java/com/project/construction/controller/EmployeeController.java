package com.project.construction.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.project.construction.exception.ResourceNotFoundException;
import com.project.construction.model.Employee;
import com.project.construction.service.EmployeeService;

@Controller
@RequestMapping("/employees/")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;

	@GetMapping("List")
	public String getAllEmployees(Model model) throws ResourceNotFoundException {
		model.addAttribute("employees", employeeService.getAllEmployees());
		return "employee/index";
	}

	@GetMapping("show/{id}")
	public String show(@PathVariable("id") long id, Model model) {
		model.addAttribute("employee", employeeService.getEmployeeById(id));
		return "employee/show";
	}

	@GetMapping("new")
	public String employeeForm(Employee employee) {
		return "employee/add-employee";
	}

	@PostMapping("add")
	public String add(@Valid Employee employee, BindingResult result, Model model) {
		if (result.hasErrors()) {
			return "add-employee";
		}
		employeeService.createEmployee(employee);
		return "redirect:List";
	}

	@GetMapping("edit/{id}")
	public String showUpdateForm(@PathVariable("id") long id, Model model) {
		Employee employee = employeeService.getEmployeeById(id);

		model.addAttribute("employee", employee);
		return "employee/update";
	}

	@PostMapping("update/{id}")
	public String update(@PathVariable("id") long id, @Valid Employee employee, BindingResult result, Model model) {
		if (result.hasErrors()) {
			employee.setId(id);
			return "employee/update";
		}

		employeeService.updateEmployee(id, employee);

		model.addAttribute("employees", employeeService.getAllEmployees());
		return "employee/index";
	}

	@GetMapping("delete/{id}")
	public String delete(@PathVariable("id") long id, Model model) {
		Employee employee = employeeService.getEmployeeById(id);

		employeeService.deleteEmployee(id, employee);

		model.addAttribute("employees", employeeService.getAllEmployees());
		return "employee/index";
	}

	// Delete a Employee
	@DeleteMapping("/employees/{id}")
	public ResponseEntity<?> delete(@PathVariable(value = "id") Long employeeId,
			@Valid @RequestBody Employee employeeDetails) {
		return employeeService.deleteEmployee(employeeId, employeeDetails);
	}

}
