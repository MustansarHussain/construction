package com.project.construction.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

@Entity
@Table(name = "employees")
public class Employee {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String name;

    
    @Column(name = "father_name", nullable = false)
    private String fatherName;
    
  
    private String cnic;
    
    private BigDecimal salary;

    
	public Employee() {
		super();
	}

	public Employee(Long id, String name, String fatherName, String cnic, BigDecimal salary, Date createdAt,
			Date updatedAt) {
		super();
		this.id = id;
		this.name = name;
		this.fatherName = fatherName;
		this.cnic = cnic;
		this.salary = salary;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFatherName() {
		return fatherName;
	}

	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	public String getCnic() {
		return cnic;
	}

	public void setCnic(String cnic) {
		this.cnic = cnic;
	}

	public BigDecimal getSalary() {
		return salary;
	}

	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}
    
}
