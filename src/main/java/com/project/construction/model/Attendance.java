package com.project.construction.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.*;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "attendance")
public class Attendance extends AuditModel {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

	private Date attendanceDate;

	private Boolean ispresent;
	
	@Column(name="over_time")
	private int overTime;
	
	private BigDecimal salary;
	
	private BigDecimal advanceAmount;
	
	private String remarks;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "employee_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
   // @JsonIgnore
	private Employee employee;
	
	@ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "site_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
	private Site site;

	public Attendance() {
		super();
	}

	public Attendance(Long id, Date attendanceDate, Boolean ispresent, int overTime, BigDecimal salary,
			BigDecimal advanceAmount, String remarks, Employee employee, Site site) {
		super();
		this.id = id;
		this.attendanceDate = attendanceDate;
		this.ispresent = ispresent;
		this.overTime = overTime;
		this.salary = salary;
		this.advanceAmount = advanceAmount;
		this.remarks = remarks;
		this.employee = employee;
		this.site = site;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getAttendanceDate() {
		return attendanceDate;
	}

	public void setAttendanceDate(Date attendanceDate) {
		this.attendanceDate = attendanceDate;
	}

	public Boolean getIspresent() {
		return ispresent;
	}

	public void setIspresent(Boolean ispresent) {
		this.ispresent = ispresent;
	}

	public int getOverTime() {
		return overTime;
	}

	public void setOverTime(int overTime) {
		this.overTime = overTime;
	}

	public BigDecimal getSalary() {
		return salary;
	}

	public void setSalary(BigDecimal salary) {
		this.salary = salary;
	}

	public BigDecimal getAdvanceAmount() {
		return advanceAmount;
	}

	public void setAdvanceAmount(BigDecimal advanceAmount) {
		this.advanceAmount = advanceAmount;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Employee getEmployee() {
		return employee;
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

	public Site getSite() {
		return site;
	}

	public void setSite(Site site) {
		this.site = site;
	}
	
}
