package com.project.construction.repository;

import com.project.construction.model.Attendance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AttendanceRepository extends JpaRepository<Attendance, Long> {
    List<Attendance> findByEmployeeId(Long EmployeeId);
    List<Attendance> findByIdAndEmployeeId(Long id, Long employeeId);

    List<Attendance> findBySiteId(Long SiteId);
    List<Attendance> findByIdAndSiteId(Long id, Long siteId);
}
