package com.project.construction.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.project.construction.model.Site;

@Repository
public interface SiteRepository extends JpaRepository<Site, Long> {

}
