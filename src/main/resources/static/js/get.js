$(document)
    .ready(
      function() {
        
        $("table tbody tr td:nth-child(6)>a").click(function(event) {
          event.preventDefault();
          console.log('Hi! I am here.');
        });
        
        $
            .getJSON(
              "/api/employees",
              function(data) {
                $
                    .each(
                      data,
                      function(index, item) {
                        let edit = "<a class='btn btn-primary' employee='5656' > <i class='fa fa-pencil-square-o'></i> "
                          + item['id'] + " </a>";
                        let dlt = "<a class='btn btn-danger'  data-toggle='modal' data-target='#deleteModal' > <i class='fa fa-trash'></i> "
                          + item['id'] + " </a>";

                        var addRow = "<tr> <td>" + item['id'] + "</td>";
                        addRow += " <td>" + item['name'] + "</td>";
                        addRow += " <td>" + item['fatherName'] + "</td>";
                        addRow += " <td>" + item['cnic'] + "</td>";
                        addRow += "<td>" + item['salary'] + "</td>";
                        addRow += " <td>" + edit + "</td>";
                        addRow += "<td>" + dlt + "</td> </tr>";

                        $("table tbody").append(addRow);
                      });
              });
      });